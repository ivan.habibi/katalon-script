import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('http://petclinic-sit.apps.ocp4.miilab.biz/owners/find')

not_run: WebUI.delay(30)

not_run: WebUI.click(findTestObject('Page_PetClinic  a Spring Framework demonstration/a_Veterinarians'))

not_run: WebUI.click(findTestObject('Object Repository/Page_PetClinic  a Spring Framework demonstration/span_Find owners'))

WebUI.click(findTestObject('Object Repository/Page_PetClinic  a Spring Framework demonstration/a_Add Owner'))

WebUI.verifyElementPresent(findTestObject('Page_PetClinic2  a Spring Framework demonstration/label_First Name'), 0)

not_run: WebUI.setText(findTestObject('Object Repository/Page_PetClinic  a Spring Framework demonstration/input_First Name_firstName'), 
    'ivan')

not_run: WebUI.setText(findTestObject('Object Repository/Page_PetClinic  a Spring Framework demonstration/input_Last Name_lastName'), 
    'habibi')

not_run: WebUI.setText(findTestObject('Object Repository/Page_PetClinic  a Spring Framework demonstration/input_Address_address'), 
    'jln tanah tinggi')

not_run: WebUI.setText(findTestObject('Object Repository/Page_PetClinic  a Spring Framework demonstration/input_City_city'), 
    'jakarta pusat')

not_run: WebUI.setText(findTestObject('Object Repository/Page_PetClinic  a Spring Framework demonstration/input_Telephone_telephone'), 
    '1321450548')

not_run: WebUI.click(findTestObject('Object Repository/Page_PetClinic  a Spring Framework demonstration/button_Add Owner'))

not_run: WebUI.verifyElementText(findTestObject('Page_PetClinic  a Spring Framework demonstration/td_ivan habibi'), 'ivan habibi')

WebUI.closeBrowser()

